// function getListUser(){
//     return new Promise((resolve, reject) =>{
//         axios.get('http://localhost/api/list-user')
//         .then(res =>{
//             console.log(res);
//             resolve(res.data);
//         })
//         .catch(err =>{
//             console.log(err);
//             reject(err);
//         })
//     })
// }

// getListUser().then(data => {
//     console.log(data);
//     return data[0];
// }).then(firstdata => {
//     setTimeout(()=>{
//     console.log(firstdata);
//     return firstdata.name;
//     },5000)
// }).then(name => {
//     console.log(name);
// })

// //cach2
// const listUser = new Promise((resolve, reject) =>{
//     axios.get('http://localhost/api/list-user')
//     .then(res =>{
//         console.log(res);
//         resolve(res.data);
//     })
//     .catch(err =>{
//         console.log(err);
//         reject(err);
//     })
// })

// listUser.then(data => {
//     console.log(data);
//     return data[0];
// }).then(firstdata => {
//     console.log(firstdata);
//     return firstdata.name;
// }).then(name => {
//     console.log(name);
// })

// //cach 3 design pattern

// function getListUser(url,method){
//     return new Promise((resolve,reject) =>{
//         instance({
//             url:url,
//             method:method,
//         })
//         .then(res =>{
//             console.log(res);
//             resolve(res.data);
//         })
//         .catch(err =>{
//             console.log(err);
//             reject(err);
//         })
//     })
// }

// getListUser('list-user','get')
// .then(data => {
//     console.log(data);
//     return data.data;
// }).then(firstdata => {
//     console.log(firstdata);
//     return firstdata[0];
// }).then(name => {
//     console.log(name.name);
// })