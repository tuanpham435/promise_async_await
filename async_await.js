// // vd1 cach viet 1
// function callApi(method,url){
//     return new Promise((resolve,reject) =>{
//         instance({
//             url:url,
//             method:method
//         }).then(res => {
//             resolve(res);
//         }).catch(err => {
//             reject(err)
//         })
//     })
// }

// callApi('get','list-user')
// .then(res => {
//     console.log(res);
//     return res.data;
// })
// .then(data => {
//     console.log(data);
//     return data[0];
// }).then(firstdata => {
//     console.log(firstdata);
//     return firstdata.name;
// }).then(name => {
//     console.log(name);
// }).catch(err => {
//     console.log(err);
// })

// //vd1 cach viet 2

// function callApi(method,url){
//     return new Promise((resolve,reject) =>{
//         resolve( instance({
//             url:url,
//             method:method
//         }));
//     })
// }

// async function getlist() {
//     try{
//         const res = await callApi('get','list-user')
//         const res2 = await new Promise((resolve,reject) =>{
//             resolve(res.data);
//         })
//         console.log(res);
//         console.log(res2);
//     }catch(err){
//         console.log(err);
//     }
// }
// getlist()

// //vd1 cach viet 3

// async function callApi(method,url) {
//     return instance({method:method,url:url})
// }

// const listuser = callApi('post','list-user');
// listuser.then(res => {
//     console.log(res);
// }).catch(err => {
//     console.log(err);
//})

// vd1 cach viet 4
async function callApi(method,url) {
    return instance({method:method,url:url})
}

async function getlist() {
    try {
        const res = await callApi('get','list-user');
        console.log(res);
    }
    catch(e) {
        console.log(e);
    }
}

getlist()

// // vd2

// function yayOrNay() {
// return new Promise((resolve, reject) => {
//     const val = Math.round(Math.random() * 1); // 0 or 1, at random

//     val ? resolve('Lucky!!') : reject('Nope 😠');
// });
// }

// async function msg() {
// try {
//     const msg = await yayOrNay();
//     console.log(msg);
// } catch(err) {
//     console.log(err);
// }
// }
// // async function msg() {
// //     const msg = await yayOrNay();
// //     console.log(msg);
// //   }
  
// // msg().catch(x => console.log(x));

// msg();

// //vd3
// async function fetchUsers(endpoint) {
//     const res = await fetch(endpoint);
//     let data = await res.json();
  
//     data = data.map(user => user.username);
  
//     console.log(data);
//   }
  
//   fetchUsers('https://jsonplaceholder.typicode.com/users');

// //vd 4
// async function myDisplay() {
// let myPromise = new Promise(function(myResolve, myReject) {
//     myResolve("I love You !!");
// });
// document.getElementById("demo").innerHTML = await myPromise;
// }

// myDisplay();

// //vd 5

// a promise
// let promise = new Promise(function (resolve, reject) {
//     setTimeout(function () {
//     resolve('Promise resolved')}, 4000); 
// });

// // async function
// async function asyncFunc() {

//     // wait until the promise resolves 
//     let result = await promise; 

//     console.log(result);
//     console.log('hello');
// }

// // calling the async function
// asyncFunc();

// //vd 6

// a promise
// let promise = new Promise(function (resolve, reject) {
//     setTimeout(function () {
//     resolve('Promise resolved')}, 4000); 
// });

// // async function
// async function asyncFunc() {
//     try {
//         // wait until the promise resolves 
//         let result = await promise; 

//         console.log(result);
//     }   
//     catch(error) {
//         console.log(error);
//     }
// }

// // calling the async function
// asyncFunc(); // Promise resolved